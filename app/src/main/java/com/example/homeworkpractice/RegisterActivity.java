package com.example.homeworkpractice;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    Button btnRegister,btnTime, btnDate;
    TextInputLayout edtFullName, edtPassword, edtEmail,edtDate,edtTime;
    RadioGroup radioGroup;
    CheckBox checkPolicy;
    RadioButton generRadioButton;

    TextView txtGetName,txtGetEmail,txtGetPassword,txtGetGender,txtGetDate,txtGetTime,txtGetAddress;

    String[] address = {"Tuol Kuk","Mean Chey", "Sen Sok","Steng Mean Chey","Kam Bo","Cham Ka Morn"};
    Spinner spinnerAddress;

    TextView txtTitle;



    String selectAddress =  "";
    int day, month, year, hour, minute;
    int myday, myMonth, myYear, myHour, myMinute;


    ActivityResultLauncher<Intent> startForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if(result != null && result.getResultCode() == RESULT_OK){
                if(result.getData() != null && result.getData().getStringExtra(GetDataActivity.KEY_NAME)!=null){
                    txtTitle.setText(result.getData().getStringExtra(GetDataActivity.KEY_NAME));
                    txtGetName.setText(result.getData().getStringExtra(GetDataActivity.KEY_NAME));
                }
                 if(result.getData() != null || result.getData().getStringExtra(GetDataActivity.KEY_EMAIL)!=null){
                    txtGetEmail.setText(result.getData().getStringExtra(GetDataActivity.KEY_EMAIL));
                }if(result.getData() != null && result.getData().getStringExtra(GetDataActivity.KEY_PASS)!=null){
                    txtGetPassword.setText(result.getData().getStringExtra(GetDataActivity.KEY_PASS));
                }
                 if(result.getData() != null && result.getData().getStringExtra(GetDataActivity.KEY_GENDER)!=null){
                    txtGetGender.setText(result.getData().getStringExtra(GetDataActivity.KEY_GENDER));
                }
                 if(result.getData() != null && result.getData().getStringExtra(GetDataActivity.KEY_DATE)!=null){
                    txtGetDate.setText(result.getData().getStringExtra(GetDataActivity.KEY_DATE));
                }
                 if(result.getData() != null && result.getData().getStringExtra(GetDataActivity.KEY_TIME)!=null){
                    txtGetTime.setText(result.getData().getStringExtra(GetDataActivity.KEY_TIME));
                }
                 if(result.getData() != null && result.getData().getStringExtra(GetDataActivity.KEY_ADDRESS)!=null){
                    txtGetAddress.setText(result.getData().getStringExtra(GetDataActivity.KEY_ADDRESS));
                }
            }
        }
    });



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txtTitle = findViewById(R.id.txtTitle);


         edtFullName = findViewById(R.id.edt_name);
         edtPassword = findViewById(R.id.edt_password);
         edtEmail = findViewById(R.id.edt_email);
         edtDate = findViewById(R.id.edt_date);
         edtTime = findViewById(R.id.edt_time);

        //        back result
        txtGetName = findViewById(R.id.txtGetName);
        txtGetEmail = findViewById(R.id.txtGetEmail);
        txtGetPassword = findViewById(R.id.txtGetPassword);
        txtGetGender = findViewById(R.id.txtGetGender);
        txtGetAddress = findViewById(R.id.txtGetAddress);
        txtGetDate = findViewById(R.id.txtGetDate);
        txtGetTime = findViewById(R.id.txtGetTime);

         checkPolicy = findViewById(R.id.cbPolicy);
        radioGroup = findViewById(R.id.radioGroup);

        spinnerAddress = findViewById(R.id.edt_address);
        spinnerAddress.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,address);
        spinnerAddress.setAdapter(aa);
        spinnerAddress.setPrompt("Choose your address");

        btnRegister = findViewById(R.id.btnRegister);
        btnDate = findViewById(R.id.btnDate);
        btnTime = findViewById(R.id.btnTime);

//Date Picker
        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this,RegisterActivity.this,year,month,day);
                datePickerDialog.show();
            }
        });

//        Time picker

        btnTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                hour = calendar.get(Calendar.HOUR);
                minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(RegisterActivity.this,RegisterActivity.this,hour,minute, DateFormat.is24HourFormat(RegisterActivity.this));
                timePickerDialog.show();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                String fullName = edtFullName.getEditText().getText().toString();
                String email = edtEmail.getEditText().getText().toString();
                String password = edtPassword.getEditText().getText().toString();
                String date = edtDate.getEditText().getText().toString();
                String time = edtTime.getEditText().getText().toString();
                boolean check = checkPolicy.isChecked();
                int selectedId = radioGroup.getCheckedRadioButtonId();


                boolean checkValidation = validationForm(fullName,email,password,selectedId,date,time);
                if(checkValidation==true){
                    generRadioButton = findViewById(selectedId);
                    bundle.putString("fullName",fullName);
                    bundle.putString("email",email);
                    bundle.putString("password",password);
                    bundle.putString("date",date);
                    bundle.putString("time",time);
                    bundle.putBoolean("policy",check);
                    bundle.putString("address",selectAddress);
                    if(selectedId != -1){
                        bundle.putString("gender",generRadioButton.getText()+"");
                    }
                    Intent intent = new Intent(RegisterActivity.this,GetDataActivity.class);
                    intent.putExtras(bundle);
//                    startActivity(intent);
                    startForResult.launch(intent);
                }else{
                    return;
                }
            }
        });



    }

    private boolean validationForm(String name,String email,String password,int gender, String date, String time){


        if(name.length()==0){
            edtFullName.requestFocus();
            edtFullName.setError("Name can not be empty");
            return false;
        }else if(email.length()==0){
            edtEmail.requestFocus();
            edtEmail.setError("Email can not be empty");
            return false;
        }else if(!email.matches("[a-zA-Z0-9._-]+@[a-z]{5}+\\.+[a-z]{3,4}")){
            edtEmail.requestFocus();
            edtEmail.setError("Please Input valid Email");
            return false;
        }
        else if(password.length()<8){
            edtPassword.requestFocus();
            edtPassword.setError("Password length must be greater than 8 ");
            return false;
        }else if(gender == -1){

            generRadioButton.setError("Please Select gender");
            return false;

        }
        else if(date.isEmpty()){
            edtDate.requestFocus();
            edtDate.setError("date can not be null");
            return false;
        }else if(time.isEmpty()){
            edtTime.requestFocus();
            edtTime.setError("time can not be null");
            return false;
        }
        else{
            return true;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
          selectAddress = adapterView.getItemAtPosition(i).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    //date
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        myYear = year;
        myMonth = month;
        myday = dayOfMonth;

        edtDate.getEditText().setText(myYear+"/"+myMonth+"/"+myday);
    }

    //Time
    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        myHour = hourOfDay;
        myMinute = minute;
        edtTime.getEditText().setText(hourOfDay+" : "+minute);
    }


    @Override
    protected void onResume() {
        super.onResume();
        edtFullName.getEditText().setText("");
        edtEmail.getEditText().setText("");
        edtPassword.getEditText().setText("");
        edtTime.getEditText().setText("");
        edtDate.getEditText().setText("");
        checkPolicy.setChecked(false);
        generRadioButton = findViewById(-1);
    }



}