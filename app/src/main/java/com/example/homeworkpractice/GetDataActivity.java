package com.example.homeworkpractice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GetDataActivity extends AppCompatActivity {
    Button btnBack,btnBackResult;
    TextView name,email,password,gender,date,time,txtAddress;

    public static final String KEY_NAME = "NAME";
    public static final String KEY_GENDER="GENDER";
    public static final String KEY_EMAIL="GENDER",KEY_PASS="PASSWORD",KEY_DATE="DATE",KEY_TIME="TIME",KEY_ADDRESS="ADDRESS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_data);

        name = findViewById(R.id.txtFullName);
        email = findViewById(R.id.txtEmail);
        password = findViewById(R.id.txtPassword);
        gender = findViewById(R.id.txtGender);
        date = findViewById(R.id.txtDate);
        time = findViewById(R.id.txtTime);
        txtAddress = findViewById(R.id.txtAddress);




        btnBack = findViewById(R.id.btnGoBack);
        btnBackResult = findViewById(R.id.btnGoBackResult);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            name.setText(bundle.getString("fullName"));
            email.setText(bundle.getString("email"));
            password.setText(bundle.getString("password"));
            gender.setText(bundle.getString("gender"));
            date.setText(bundle.getString("date"));
            time.setText(bundle.getString("time"));
            txtAddress.setText(bundle.getString("address"));
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnBackResult.setOnClickListener(new View.OnClickListener() {
            Bundle mBundle = getIntent().getExtras();
            @Override
            public void onClick(View view) {
                String name = mBundle.getString("fullName");
                String email = mBundle.getString("email");
                Intent intent = new Intent();
                intent.putExtras(bundle);
                intent.putExtra(KEY_NAME,name);
                intent.putExtra(KEY_EMAIL,email);
                intent.putExtra(KEY_PASS,mBundle.getString("password"));
                intent.putExtra(KEY_GENDER,mBundle.getString("gender"));
                intent.putExtra(KEY_DATE,mBundle.getString("date"));
                intent.putExtra(KEY_TIME,mBundle.getString("time"));
                intent.putExtra(KEY_ADDRESS,mBundle.getString("address"));
                setResult(RESULT_OK,intent);

                finish();
            }
        });
    }
}